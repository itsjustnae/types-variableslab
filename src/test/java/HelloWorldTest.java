import org.junit.Test;

import static org.junit.Assert.*;


public class HelloWorldTest {

    private HelloWorld helloWorld = new HelloWorld();

    @Test
    public void checkIntegerDivision() {
        assertEquals(2, 5 / 2);
    }

    @Test
    public void checkFloatingPointDivsion() {
        assertEquals(2.5, 5/2., 0.01);
    }

    @Test
    public void isLengthGreaterThan() {

        assertTrue(helloWorld.isLengthGreaterThan("hello", 5));
    }

    @Test
    public void verifyMethodWorksForNulls() {
        assertFalse(helloWorld.isLengthGreaterThan(null, 5));
    }
}